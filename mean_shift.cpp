#include "mean_shift.h"
#include <cmath>
#include <memory>
#include <numeric>

std::function<double(point)> make_flat_kernel(double lambda, point reference) {
    return [lambda, reference](point point) {
        if (point.distance_from(reference) <= lambda) {
            return 1.0;
        } else {
            return 0.0;
        }
    };
}

std::function<double(point)> make_gaussian_kernel(double sigma, point reference) {
    return [sigma, reference](point point) {
        return std::exp(-(point - reference).norm() / (2 * std::pow(sigma, 2)));
    };
}

point shift(const std::vector<point>& points, const point& seed, const std::function<std::function<double(point)>(double, point)>& kernel_generator, double bandwidth, double epsilon, int max_iter) {
    int num_iter = 0;
    point result = seed;

    while (num_iter < max_iter) {
        std::vector<point> neighbors{};
        std::function<double(point)> kernel = kernel_generator(bandwidth, result);

        std::copy_if(points.begin(), points.end(), std::back_inserter(neighbors), [&kernel](point s) { return kernel(s) != 0; });

        std::vector<point> numerators(neighbors);
        std::vector<double> denominator(neighbors.size(), 0.0);

        for (std::vector<point>::size_type i = 0; i < neighbors.size(); i++) {
            numerators[i] = kernel(neighbors[i]) * neighbors[i];
        }

        for (std::vector<point>::size_type i = 0; i < neighbors.size(); i++) {
            denominator[i] = kernel(neighbors[i]);
        }

        if (denominator.empty()) {
            denominator.push_back(1.0);
        }

        std::unique_ptr<point> numerator;
        if (!numerators.empty()) {
            numerator = std::make_unique<point>(numerators[0]);

            for (std::vector<point>::size_type i = 1; i < numerators.size(); i++) {
                *numerator += numerators[i];
            }
        } else {
            unsigned int dimensions = points[0].dimensions();
            numerator = std::make_unique<point>(std::vector<double>(dimensions, 0.0));
        }

        double denumerator = 0.0;
        for (std::vector<double>::size_type i = 0; i < denominator.size(); i++) {
            denumerator += denominator[i];
        }

        point mean_shift = *numerator / denumerator;

        if (mean_shift.distance_from(result) < epsilon) {
            break;
        }

        result = mean_shift;
        num_iter++;
    }

    return result;
}

std::pair<std::vector<point>, std::unordered_map<int, std::vector<int>>> mean_shift(const std::vector<point>& data, const std::function<std::function<double(point)>(double, point)>& kernel_generator, double bandwidth, double epsilon, int max_iter) {
    std::vector<point> current_iteration{data};

    #ifdef _MS_USE_OPENMP
    #pragma omp parallel for default(none) shared(current_iteration, kernel_generator, bandwidth, epsilon, max_iter) schedule(dynamic)
    #endif
    for (std::vector<point>::size_type i = 0; i < current_iteration.size(); ++i) {
        current_iteration[i] = shift(current_iteration, current_iteration[i], kernel_generator, bandwidth, epsilon, max_iter);
    }

    std::vector<point> centroids{current_iteration};
    std::unordered_map<int, std::vector<int>> clusters;

    for (std::vector<point>::size_type i = 0; i < current_iteration.size(); ++i) {
        clusters.insert({i, std::vector<int>{static_cast<int>(i)}});
    }

    for (std::vector<point>::size_type i = 0; i < current_iteration.size(); ++i) {
        for (std::vector<point>::size_type j = 0; j < i; ++j) {
            if (current_iteration[i].distance_from(current_iteration[j]) < epsilon) {
                clusters[i].insert(clusters[i].end(), clusters[j].begin(), clusters[j].end());

                clusters.erase(j);

                auto pos_j = std::find(centroids.begin(), centroids.end(), current_iteration[j]);
                if (pos_j != centroids.end() && centroids.size() > 1) {
                    centroids.erase(pos_j);
                }
            }
        }
    }

    return std::make_pair(centroids, clusters);
}
