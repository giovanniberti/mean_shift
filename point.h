#pragma once

#include <ostream>
#include <vector>
#include <iterator>
#include <iostream>

class point {
public:
    point(std::vector<double> components);

    double norm() const;

    double distance_from(const point& other) const;

    unsigned int dimensions() const;

    const std::vector<double>& get_components() const;

    bool operator==(const point& rhs) const;

    bool operator!=(const point& rhs) const;

    friend std::ostream& operator<<(std::ostream &out, const point& p);

    point& operator+=(const point& other) {
        if (other.components.size() != components.size()) {
            throw std::logic_error("components size mismatch this: " + std::to_string(components.size()) + " other: " + std::to_string(other.components.size()));
        }

        for (std::vector<double>::size_type i = 0; i < components.size(); i++) {
            components[i] += other.components[i];
        }

        return *this;
    }

    friend point operator+(point lhs, const point& rhs) {
        lhs += rhs;
        return lhs;
    }

    point& operator-=(const point& other) {
        if (other.components.size() != components.size()) {
            throw std::logic_error("components size mismatch this: " + std::to_string(components.size()) + " other: " + std::to_string(other.components.size()));
        }

        for (std::vector<double>::size_type i = 0; i < components.size(); i++) {
            components[i] -= other.components[i];
        }

        return *this;
    }

    friend point operator-(point lhs, const point& rhs) {
        lhs -= rhs;
        return lhs;
    }

    point& operator*=(const double& lambda) {
        for (std::vector<double>::size_type i = 0; i < components.size(); i++) {
            components[i] *= lambda;
        }

        return *this;
    }

    friend point operator*(point lhs, const double rhs) {
        lhs *= rhs;
        return lhs;
    }

    friend point operator*(const double lhs, point rhs) {
        rhs *= lhs;
        return rhs;
    }

    point& operator/=(const double lambda) {
        for (std::vector<double>::size_type i = 0; i < components.size(); i++) {
            components[i] /= lambda;
        }

        return *this;
    }

    friend point operator/(point lhs, const double rhs) {
        lhs /= rhs;
        return lhs;
    }

    friend point operator/(const double lhs, point rhs) {
        rhs /= lhs;
        return rhs;
    }

private:
    std::vector<double> components;
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
    out << "[";
    if(!v.empty()) {
        std::copy(v.begin(),
                  --v.end(),
                  std::ostream_iterator<T>(out, ", "));
    }

    return out << v.back() << "]";
}
