#pragma once

#include <functional>
#include <vector>
#include "point.h"

std::function<double(point)> make_flat_kernel(double lambda, point reference);

std::function<double(point)> make_gaussian_kernel(double sigma, point reference);

std::pair<std::vector<point>, std::unordered_map<int, std::vector<int>>> mean_shift(const std::vector<point>& data, const std::function<std::function<double(point)>(double, point)>& kernel_generator, double bandwidth, double epsilon, int max_iter = 100000);
