#include <cmath>
#include <utility>
#include <algorithm>
#include "point.h"

point::point(std::vector<double> components) : components(std::move(components)) {}

double point::norm() const {
    double sum = 0.0;
    for (auto component : components) {
        sum += std::pow(component, 2);
    }

    return sqrt(sum);
}

double point::distance_from(const point& other) const {
    return (*this - other).norm();
}

unsigned int point::dimensions() const {
    return components.size();
}

std::ostream& operator<<(std::ostream& out, const point& p) {
    return out << "point(" << p.components << ")";
}

bool point::operator==(const point &rhs) const {
    return components == rhs.components;
}

bool point::operator!=(const point &rhs) const {
    return !(rhs == *this);
}

const std::vector<double>& point::get_components() const {
    return components;
}
