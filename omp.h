#pragma once

namespace omp {
    template<class InputIt, class OutputIt, class UnaryOperation>
    OutputIt transform(InputIt first1, InputIt last1, OutputIt d_first,
                       UnaryOperation unary_op) {
        int count = std::distance(first1, last1);

        #pragma omp for private(d_first, first1)
        for (int i = 0; i < count; ++i) {
            *d_first++ = unary_op(*first1++);
        }

        return d_first;
    }

    template<class InputIt, class T>
    T accumulate(InputIt first, InputIt last, T init) {
        int count = std::distance(first, last);

        #pragma omp for private(first)
        for (int i = 0; i < count; ++i) {
            init = init + *first;
            ++first;
        }

        return init;
    }
}