#include <iostream>
#include <vector>
#include <fstream>
#include <chrono>
#include "point.h"
#include "mean_shift.h"
#include "popl.h"

using popl::OptionParser;
using popl::Switch;
using popl::Value;

std::vector<std::string> split_string(std::string input, std::string delimiter) {
    auto start = 0U;
    auto end = input.find(delimiter);
    auto result = std::vector<std::string>{};

    while (end != std::string::npos) {
        result.push_back(input.substr(start, end - start));
        start = end + delimiter.length();
        end = input.find(delimiter, start);
    }

    result.push_back(input.substr(start, end));

    return result;
}

std::vector<point> parse_input(const std::string& file_path) {
    std::vector<point> points;
    std::ifstream input_file{file_path};
    std::string line;

    if (!input_file) {
        throw std::runtime_error("Unable to read input file");
    }

    while (std::getline(input_file, line)) {
        auto string_components = split_string(line, " ");
        auto components = std::vector<double>{};

        std::transform(string_components.begin(), string_components.end(), std::back_insert_iterator(components), [](std::string component) {
            return std::stod(component);
        });

        points.emplace_back(components);
    }

    return points;
}

void write_clusters_to_file(const std::string& output_path, const std::vector<point>& dataset, std::pair<std::vector<point>, std::unordered_map<int, std::vector<int>>> cluster_data) {
    std::ofstream output_file{output_path};

    std::vector<point> centroids;
    std::unordered_map<int, std::vector<int>> clusters;
    std::tie(centroids, clusters) = cluster_data;

    for (std::vector<point>::size_type i = 0; i < dataset.size(); i++) {
        const point& p = dataset[i];
        const std::vector<double>& components = p.get_components();

        for (unsigned int dim = 0; dim < p.dimensions(); dim++) {
            output_file << components[dim] << " ";
        }

        for (auto const& key_value : clusters) {
            if (std::find(key_value.second.begin(), key_value.second.end(), i) != key_value.second.end()) {
                output_file << key_value.first;
                break;
            }
        }

        output_file << "\n";
    }
}

int main(int argc, char **argv) {
    #ifdef _MS_USE_OPENMP
    std::cout << "-- OpenMP parallel version --" << std::endl;
    #else
    std::cout << "-- Serial version --" << std::endl;
    #endif

    OptionParser optionParser("Options (default parameters between parentheses)");
    auto help_option = optionParser.add<Switch>("h", "help", "");
    auto input_file_option = optionParser.add<Value<std::string>>("i", "input", "absolute path of input file for dataset. Format: space separated values, each row is a point.");
    auto bandwidth_option = optionParser.add<Value<double>>("b", "bandwidth", "set bandwidth parameter for kernel function", 2.0);
    auto kernel_option = optionParser.add<Value<std::string>>("k", "kernel", "set kernel function to use. Available options: flat, gaussian", "flat");
    auto tolerance_option = optionParser.add<Value<double>>("t", "tolerance", "set tolerance before considering a point to have converged", 0.01);
    auto max_iter_option = optionParser.add<Value<int>>("n", "num-iterations", "set maximum number of iterations before declaring convergence", 1000);
    auto output_file_option = optionParser.add<Value<std::string>>("o", "output", "absolute path of output file for clusters");

    optionParser.parse(argc, argv);

    if (help_option->is_set()) {
        std::cout << optionParser << std::endl;
        std::exit(0);
    }

    std::function<std::function<double(point)>(double, point)> kernel_generator;
    if (kernel_option->value() == "flat") {
        kernel_generator = make_flat_kernel;
    } else if (kernel_option->value() == "gaussian") {
        kernel_generator = make_gaussian_kernel;
    } else {
        std::cerr << "Invalid kernel parameter. Valid values: flat, gaussian." << std::endl;
        std::exit(-1);
    }


    double epsilon = tolerance_option->value();
    double bandwidth = bandwidth_option->value();
    int max_iterations = max_iter_option->value();

    // clusters = [[2, 2, 2], [7, 7, 7], [5, 13, 13]]
    std::vector<point> input{parse_input(input_file_option->value())};

    std::cout << "parsed input (" << input.size() << " points)" << std::endl;

    //std::vector<point> centroids{mean_shift(input, make_gaussian_kernel, 0.45, 1000)};
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    std::pair<std::vector<point>, std::unordered_map<int, std::vector<int>>> cluster_data{mean_shift(input, kernel_generator, bandwidth, epsilon, max_iterations)};
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    std::vector<point> centroids;
    std::unordered_map<int, std::vector<int>> clusters;
    std::tie(centroids, clusters) = cluster_data;

    int ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
    std::cout << "Found " << centroids.size() << " centroids in " << ms << " ms:" << std::endl;

    for (auto &p : centroids) {
        std::cout << p << std::endl;
    }

    if (output_file_option->is_set()) {
        std::string output_path = output_file_option->value();
        std::cout << "Writing clusters to output file: " << output_path << std::endl;
        write_clusters_to_file(output_path, input, cluster_data);
    }
}
